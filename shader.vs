/**********************************
*         uniform variables
**********************************/
// texture & lighting
uniform bool uUseTexture;
uniform bool uUseLighting;

// model view & projection matrix
uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat3 uNMatrix;

// light0
uniform bool uLight0Enabled;
uniform vec4 uLight0Pos;
uniform vec4 uLight0Ambient;
uniform vec4 uLight0Diffuse;
uniform vec4 uLight0Specular;

// light1
uniform bool uLight1Enabled;
uniform vec4 uLight1Pos;
uniform vec4 uLight1Ambient;
uniform vec4 uLight1Diffuse;
uniform vec4 uLight1Specular;

// eye position
uniform vec3 uEyePos;

/**********************************
*         attribute variables
**********************************/
// vertex, color, texture, normal
attribute vec3 aVertexPosition;
attribute vec4 aVertexColor;
attribute vec2 aTextureCoord;
attribute vec3 aVertexNormal;

/**********************************
*         varying variables
**********************************/
varying vec2 vTextureCoord;
varying vec3 vLightWeight;

/**********************************
*                main
**********************************/
void main(void)
{
    vec4 mvPosition = uMVMatrix * vec4(aVertexPosition, 1.0);
    gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
    // if texture is used
    if(uUseTexture)
    {
        vTextureCoord = aTextureCoord;
    }
    
    // if lighting is used
    if(uUseLighting)
    {
        vLightWeight = vec3(0.0, 0.0, 0.0);
        vec3 eyeDir = normalize(uEyePos - mvPosition.xyz);
        
        // light0: direction
        if(uLight0Enabled)
        {
            vec3 lightDir = normalize(uLight0Pos.xyz);
            vec3 transformedNormal = normalize(uNMatrix * aVertexNormal);
            // diffuse
            float DiffuseWeight = clamp(dot(transformedNormal, lightDir), 0.0, 1.0);
            // specular
            vec3 reflectionDir = reflect(-lightDir, transformedNormal);
            float specularWeight = pow(max(dot(reflectionDir, eyeDir), 0.0), 10.0);
            
            vLightWeight += uLight0Ambient.rgb + uLight0Diffuse.rgb * DiffuseWeight +
                            uLight0Specular.rgb * specularWeight;
        }
        
        // light1: point
        if(uLight1Enabled)
        {

            vec3 lightDir = normalize(uLight1Pos.xyz - mvPosition.xyz); 
            vec3 transformedNormal = normalize(uNMatrix * aVertexNormal);
            // diffuse
            float DiffuseWeight = clamp(dot(transformedNormal, lightDir), 0.0, 1.0);
            // specular
            vec3 reflectionDir = reflect(-lightDir, transformedNormal);
            float specularWeight = pow(max(dot(reflectionDir, eyeDir), 0.0), 10.0);
            
            vLightWeight += uLight1Ambient.rgb + uLight1Diffuse.rgb * DiffuseWeight +
                            uLight1Specular.rgb * specularWeight;
        }
    }
    else
    {
        vLightWeight = vec3(1.0, 1.0, 1.0);
    }
}