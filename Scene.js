 /**
  * Scene class:
  *   Control objects drawing, lighting, viewing
  * @author Xiaobo
  * Created: 11/18/2013
  */
 function Scene(sceneName)
 {
    var _sceneName = sceneName;
    
	var _objects = Array();
	var _isDrawAxis = false;
	var _isDrawNormals = false;
	var _isDrawLightObject = false;
	var _eye = new Point3D(5, 5, 10);
	var _lookat = new Point3D(0, 0, 0);
	var _lookup = new Point3D(0, 1, 0);
	
	var _viewAngle = 45;
	var _lights = new LightControl();
	
	var _pointLightObject = null;
	var _directionLightObject = null;
	var _sceneMode = Scene.MODE_NORMAL;
	
	/*****************************************
    *         get scene name
    ***************************************/
	this.getSceneName = function()
	{
	    return _sceneName;
	};
	
	/*****************************************
    *         set scene mode
    ***************************************/
	this.setSceneMode = function(mode)
	{
	    _sceneMode = mode;
	};
	
	/*****************************************
    *         get scene mode
    ***************************************/
	this.getSceneMode = function()
	{
	    return _sceneMode;
	};
	
	/*****************************************
    *         initialize the scene
    ***************************************/
	this.init = function()
	{
	    for(var i=0; i<_objects.length; i++)
        {
            _objects[i].init();
        }
	    
	    createPointLightObject();
	};
	
	/*****************************************
    *         set if draw normals
    ***************************************/
	this.setDrawNormals = function(value)
	{
	    _isDrawNormals = value;
	};
	
	/*****************************************
    *         get if draw normals
    ***************************************/
    this.isDrawNormals = function()
    {
        return _isDrawNormals;
    };
	
	/*****************************************
    *         set if draw axis
    ***************************************/
	this.setDrawAxis = function(value)
	{
	    _isDrawAxis = value;
	};
	
	/*****************************************
    *         get if draw axis
    ***************************************/
    this.isDrawAxis = function()
    {
        return _isDrawAxis;
    };
    
    /*****************************************
     *         set if draw axis
     ***************************************/
 	this.setDrawLight = function(value)
 	{
 		_isDrawLightObject = value;
 	};
 	
 	/*****************************************
     *         get if draw axis
     ***************************************/
     this.isDrawLight = function()
     {
         return _isDrawLightObject;
     };
	
	/*****************************************
	*         setup view
	***************************************/
	function setView()
	{
		var gl = Opengl.gl;
		gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        mat4.perspective(_viewAngle, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, Opengl.pMatrix);
		var lookatMatrix = mat4.lookAt(_eye.getArray(), _lookat.getArray(), _lookup.getArray());
		Opengl.pMatrix = mat4.multiply(Opengl.pMatrix, lookatMatrix);
		mat4.identity(Opengl.mvMatrix);
        //Opengl.mvMatrix = mat4.lookAt(_eye.getArray(), _lookat.getArray(), _lookup.getArray());
        Opengl.setMatrixUniforms();
        
        Opengl.setEyePosUniforms(_eye.getArray());
	}
	
	/*****************************************
    *         setup lighting
    ***************************************/
    function setLighting()
    {
        Opengl.gl.uniform1i( ShaderControl.shaderProgram.useLightingUniform, true );
        _lights.update();
    };
    
    this.getLights = function()
    {
        return _lights;
    };
	
	/*****************************************
	*        add object to the scene
	***************************************/
	this.add = function(obj)
	{
		_objects.push(obj);
	};
	
	/*****************************************
	*       get active object (last added)
	***************************************/
	this.getActiveObject = function(obj)
	{
		return _objects[_objects.length - 1];
	};

	/*****************************************
	*   draw all objects in the scene
	***************************************/
    this.redraw = function() 
    {
		setView();
		setLighting();
       
       	if(_isDrawAxis)
       	{ 
        	drawAxis();
        }
       	
       	updatePointLightObjectPos();
       	
       	if(_isDrawLightObject)
       	{
       		_pointLightObject.redraw();
       	}
       	
        for(var i=0; i<_objects.length; i++)
        {
        	if(_objects[i].setDrawNormals)
        	{
        		_objects[i].setDrawNormals(_isDrawNormals);
        	}
        	_objects[i].redraw();
        }
    };
    
    /*****************************************
	*   update position of point light object
	***************************************/
    function updatePointLightObjectPos()
    {
    	// get position of light1
        var pos = _lights.getLight(1).getPos();
        _pointLightObject.setLocation(pos[0], pos[1], pos[2]);
    }
    
    /*****************************************
	*   create  object for light1
	***************************************/
    function createPointLightObject()
    {
    	// draw a ball
    	var xs = [0.1, 0.7, 1, 0.7, 0.1];
        var ys = [-1, -0.7, 0, 0.7, 1];
       
        _pointLightObject = new SweepSurface(xs, ys, 5);
        
        _pointLightObject.setSize(0.1, 0.1, 0.1);

        _pointLightObject.setSolid(true);
        _pointLightObject.setColor(Color.white);
        updatePointLightObjectPos();
        
        _pointLightObject.init();
    }
    
    /*****************************************
	*   create  object for direction light
	*****************************************/
    function createDirectionLightObject()
    {
    
    }
    
    function drawAxis()
    {
    	var gl = Opengl.gl;
        var vertices = [
        	0, 0, 0,
        	10, 0, 0,
        	0, 0, 0,
        	0, 10, 0,
        	0, 0, 0,
        	0, 0, 10
        ];
        var normals = [
        1,1,1,
        0,0,1,
        1,1,1,
        0,0,1,
        1,1,1,
        1,0,0
        ];
        
        var vertexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
      
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
        vertexBuffer.itemSize = 3;
        vertexBuffer.numItems = 6;
        
        gl.vertexAttribPointer(ShaderControl.shaderProgram.vertexPositionAttribute, vertexBuffer.itemSize, gl.FLOAT, false, 0, 0);
        var normalBuffer = Opengl.createArrayBuffer(normals, 3, 6);
        gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
        gl.vertexAttribPointer(ShaderControl.shaderProgram.vertexNormalAttribute, normalBuffer.itemSize, gl.FLOAT, false, 0, 0);
        
		Opengl.setColorUniforms([1, 1, 1, 1]);
        gl.lineWidth(3);
        gl.drawArrays(gl.LINES, 0, vertexBuffer.numItems);
    }
    
    this.setEyeX = function(value)
    {
    	_eye.x  = value;	
    };
    
    this.setEyeY = function(value)
    {
    	_eye.y = value;
    };
    
    this.setEyeZ = function(value)
    {
    	_eye.z = value;
    };
    
    this.setEyePos = function(x, y, z)
    {
        _eye.setValues(x, y, z);
    };
    
    this.getEyePos = function (value)
    {
    	return _eye;
    };
    
    this.getViewAngle = function()
    {
    	return _viewAngle;
    };
    
    this.setViewAngle = function(value)
    {
    	_viewAngle = value;
    };
}
 
Scene.MODE_NORMAL = 0;
Scene.MODE_MULTI_TEXTURE = 1;


