/**
 * MouseHandler class
 * @author Xiaobo Sun
 * Created: 11/18/2013
 */

var MouseHandler = new function()
{
	var _startX = 0;
	var _startY = 0;
	var _dragging = false;

	function getMousePos(evt)
	{
		var rect = Canvas.canvas.getBoundingClientRect();
		var xVal = evt.clientX - rect.left;
		var yVal = evt.clientY - rect.top;
		
		return {x:xVal, y:yVal};
	};
	
	/*****************************************
	*   handle mouse move event
	***************************************/
	this.onMouseMove = function(evt)
	{
		var pos = getMousePos(evt);
		var curScene = SceneControl.curScene();
		
		 if ( _dragging )
            {
                var dx = pos.x - _startX;
                var dy = pos.y - _startY;

                var eyeX = _startEyeX + dx / 50;
                var eyeY = _startEyeY + dy / 50;
                curScene.setEyeX( eyeX );
                curScene.setEyeY( eyeY );
                curScene.redraw();
            }
            
	};
	
	/*****************************************
	*   handle mouse Down
	***************************************/
	this.onMouseDown = function(evt)
	{
		var pos = getMousePos(evt);
		
		var curScene = SceneControl.curScene();

	    _dragging = true;
	    _startX = pos.x;
	    _startY = pos.y;
	    _startEyeX = curScene.getEyePos().x;
	    _startEyeY = curScene.getEyePos().y;
		
	};
	
	/*****************************************
	*   handle mouse Down
	***************************************/
	this.onMouseUp = function(evt)
	{
		_dragging = false;
	};
	
	/*****************************************
	*   handle mouse wheel
	***************************************/
	this.onMouseWheel = function(evt)
	{
		var delta = evt.wheelDelta / 10;
		var curScene = SceneControl.curScene();
		
		var angle = curScene.getViewAngle();
		curScene.setViewAngle(angle + delta);
		curScene.redraw();
		Canvas.print(delta + "\n");
	};
	
	/*****************************************
	*   handle mouse wheel
	***************************************/
	this.onDOMMouseScroll = function(evt)
	{
		var delta = evt.detail;
		var curScene = SceneControl.curScene();
		
		var angle = curScene.getViewAngle();
		curScene.setViewAngle(angle + delta);
		curScene.redraw();
		Canvas.print(delta + "\n");
	};
	
};
