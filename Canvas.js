/**
 * Canvas class: opengl canvas
 * @author Xiaobo Sun
 * Created: 11/18/2013
 */

var Canvas = new function()
{
	this.canvas = null;
	var gl = null;

	/**
	 * initialize opengl
	 */
	this.initGL = function() 
	{
        try {
        	
            gl = this.canvas.getContext("webgl");

            gl.viewportWidth = this.canvas.width;
            gl.viewportHeight = this.canvas.height;
            gl.clearColor(0.0, 0.0, 0.0, 1.0);
            gl.enable(gl.DEPTH_TEST);
            
            Opengl.gl = gl;
            
        } catch (e) 
        {
        }
        if (!gl) {
            alert("Could not initialise WebGL, sorry :-(");
            return false;
        }
        
        return true;
    };
    
    /**
     * resize the canvas size
     */
    this.resize = function(w, h)
    {
    	this.canvas.width = w;
    	this.canvas.height = h;
    	gl.viewportWidth = w;
        gl.viewportHeight = h;
        
        SceneControl.curScene().redraw();
    };

    /**
     * print message in the debug textarea
     */
	this.print = function(str)
	{
		var debug = document.getElementById("debug");
		debug.innerHTML += str;
	};
	
	/**
	 * initialize the canvas
	 */
    this.initCanvas = function() 
    {
        this.canvas = document.getElementById("canvas");
        
        this.canvas.addEventListener('mousemove', MouseHandler.onMouseMove);
        this.canvas.addEventListener('mousedown', MouseHandler.onMouseDown);
        this.canvas.addEventListener('mouseup', MouseHandler.onMouseUp);
        this.canvas.onmousewheel = MouseHandler.onMouseWheel;
        this.canvas.addEventListener('DOMMouseScroll', MouseHandler.onDOMMouseScroll);
    };
};
