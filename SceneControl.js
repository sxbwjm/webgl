 /**
  * SceneControl class: singleton design pattern
  *   Control multiple scenes
  * @author Xiaobo
  * Created: 11/18/2013
  */
 var SceneControl = new function()
 {
 	var _scenes = Array();
 	var _curSceneIdx = -1;
 	var _curScene = null;
 	
 	// create scenes
 	
 	/*****************************************
	*            add scene
	***************************************/
 	function add(scene)
 	{
 		_scenes.push(scene);
 	};
 	
 	/*****************************************
	*            get next scene
	***************************************/
 	this.nextScene = function()
 	{
 		_curSceneIdx++;
 		if(_curSceneIdx >= _scenes.length)
 		{
 			_curSceneIdx = 0;
 		}
 		
 		_curScene = _scenes[_curSceneIdx];
 		return _curScene;
 	};
 	
 	/*****************************************
	*            get previous scene
	***************************************/
 	this.preScene = function()
 	{
 		_curSceneIdx--;
 		if(_curSceneIdx < 0)
 		{
 			_curSceneIdx = _scenes.length - 1;
 		}
 		
 		_curScene = _scenes[_curSceneIdx];
 		//alert(_curSceneIdx);
 		return _curScene;
 	};
 	
 	/*****************************************
	*            get current scene
	***************************************/
 	this.curScene = function()
 	{
 		return _curScene;
 	};
 	
 	/*****************************************
	*            create scenes
	***************************************/
 	this.createScenes = function()
 	{
 		//createTriangleScene();
 		
 		createSweepSurfaceScene1();
 		
 		createSweepSurfaceScene2();
 		createSweepSurfaceScene3();
 		createSweepSurfaceScene4();
 		createSweepSurfaceScene5();
 		
 		//createTestScene();
 	};
 	
 	/*****************************************
	*            create triangle scene
	***************************************/
 	function createTriangleScene()
 	{
 		var scene = new Scene();
 		
 		 var vertices = [
             0.0,  0.0,  0.0,
             1.0, 0.0,  0.0,
             0.5, 1.0,  0.0
        ];

		var obj = new Triangle(vertices);
		obj.setLocation(2, 0, 0);
		obj.setSize(1, 2, 1);
		obj.setRotate(45, 0, 0, 1);
		obj.setColor(Color.blue);
		scene.add(obj);
		
		obj = new Triangle(vertices);
		obj.setLocation(0, 0, 0);
		obj.setSize(1, 2, 1);
		obj.setRotate(90, 0, 0, 1);
		obj.setColor(Color.white);
		scene.add(obj);
        
        scene.setEyePos(0, 0, 10);
        
		add(scene);
 	};
 	
 	function createSweepSurfaceScene1()
 	{
 	    var scene = new Scene("wire vs solid");

 	    var xs = [1, 2, 1, 2];
        var ys = [0, 1, 2, 3];
 	    var obj = new SweepSurface(xs, ys, 20);
 	    obj.setLocation(-3, 0, 0);
 	    obj.setSolid(true);
 	    obj.setColor(Color.white);
 	    scene.add(obj);
 	    
        var obj = new SweepSurface(xs, ys, 20);
        obj.setLocation(2, 0, 0);
        obj.setSolid(false);
        obj.setColor(Color.white);
        scene.add(obj);
 	    
 	    add(scene);
 	    
 	}
 	
 	function createSweepSurfaceScene2()
    {
        var scene = new Scene("color & lighting");

        var xs = [ 0.0,  0.5,   0.7, 0.85, 1.0, 0.85, 0.7, 0.5, 0.0];//[0.5, 0.75, 1.0, 0.75, 0.5];
        var ys = [-1.0, -0.85, -0.7, -0.5, 0.0, 0.5,  0.7, 0.85,1.0];//[0.0, 0.3, 0.6, 0.9, 1.2];
        var obj = new SweepSurface(xs, ys, 20);
        obj.setSolid( true );
        obj.setLocation( 2.0, 0.0, 0.0 );
        obj.setColor( Color.white );
        scene.add(obj);
        
        obj = new SweepSurface(xs, ys, 20);
        obj.setSolid( true );
        obj.setLocation( -1.0, 0.0, -2.0 );
        obj.setColor( Color.blue );
        scene.add(obj);
        
        obj = new SweepSurface(xs, ys, 20);
        obj.setSolid( true );
        obj.setLocation( 3.0, 0.0, 3.0 );
        obj.setColor( Color.red );
        scene.add(obj);
        
        scene.getLights().getLight(1).setEnable(false);
        add(scene);
        
    }
 	
 	function createSweepSurfaceScene3()
    {
        var scene = new Scene("transformation");

        var xs = [0.5, 0.4, 0.3, 0.4, 0.5, 0.4, 0.3];
        var ys = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2];
        var obj = new SweepSurface(xs, ys, 20);
        obj.setSolid( true );
        obj.setLocation( 2.0, 0.0, 0.0 );
        obj.setColor( Color.white );
        obj.setRotate( 90, 0, 0, 1 );
        scene.add(obj);
        
        obj = new SweepSurface(xs, ys, 20);
        obj.setSolid( true );
        obj.setLocation( -1.0, 0.0, -2.0 );
        obj.setColor( Color.blue );
        obj.setSize(1.0, 2.0, 1.0);
        scene.add(obj);
        
        obj = new SweepSurface(xs, ys, 20);
        obj.setSolid( true );
        obj.setLocation( 3.0, 0.0, 3.0 );
        obj.setColor( Color.red );
        obj.setSize(1.0, 0.5, 1.0);
        obj.setRotate( 90, 1, 0, 0 );
        scene.add(obj);
        
        add(scene);
        
    }
    
    function createSweepSurfaceScene4()
    {
        var scene = new Scene("single texture");

        var xs = [1, 1, 1];
        var ys = [0, 1.5, 3];
        var textures = new TextureControl();
        var obj = new SweepSurface(xs, ys, 20);
        obj.setLocation(-2, 0, 0);
        obj.setRotate(90, 0, 1, 0);
        obj.setSolid(true);
        obj.setColor(Color.white);
        textures.addTexture("doraemon.jpg");
        obj.setTextures(textures);
        scene.add(obj);
        
        xs = [1.5, 1, 1.5, 1.3];
        ys = [0, 1, 2, 3, 4];
        obj = new SweepSurface(xs, ys, 20);
        obj.setLocation(4, 0, 0);

        obj.setSolid(true);
        obj.setColor(Color.white);
        textures = new TextureControl();
        textures.addTexture("dog.jpg");
        obj.setTextures(textures);
        scene.add(obj);
        
        add(scene);
        
    }
    
     function createSweepSurfaceScene5()
    {
        var scene = new Scene("mutiple textures");

        var xs = [1, 1, 1];
        var ys = [0, 1.5, 3];
        var textures = new TextureControl();
        var obj = new SweepSurface(xs, ys, 20);
        obj.setLocation(-1, -2, 2);
        obj.setSize(1, 2, 3);
        obj.setRotate(90, 0, 1, 0);
        obj.setSolid(true);
        obj.setColor(Color.white);
        textures.addTexture("foreground.jpg");
        textures.addTexture("background.jpg");
        textures.addTexture("title.jpg");
        
        obj.setTextures(textures);
        scene.add(obj);
        scene.setSceneMode(Scene.MODE_MULTI_TEXTURE);
        scene.setEyePos(0, 5, 11);
        add(scene);
        
    }
 	
 	/*****************************************
	*            create a test scene
	***************************************/
 	function createTestScene()
 	{
 		var scene = new Scene("test scene");
 		
        var obj = new Cube();
        obj.setLocation(3, 0, 0);
		obj.setColor(Color.green);
		//obj.setRotate(45, 0, 1, 0);
		scene.add(obj);
		
		
		obj = new Cube();
        obj.setLocation(-1, 0, 0);
		obj.setColor(Color.red);
		//obj.setRotate(60, 0, 0, 1);
		scene.add(obj);
		
		
		scene.setEyePos(0, 0, 10);
		add(scene);
 	};
 };
