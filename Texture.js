/**
 * Texture class: Manage texture
 * @author Xiaobo Sun
 * created: 11/23/2013
 */
function Texture( img, textureId )
{
    var _img = img;
    var _texture = null;
    
    var _textureId = textureId;

    /*****************************************
     *          initiate texture
     ***************************************/
    this.init = function( )
    {
        gl = Opengl.gl;
        //create texture
        _texture = gl.createTexture( );
        _texture.image = TextureImages.getImage( _img );
        gl.bindTexture( gl.TEXTURE_2D, _texture );
        
        // reverse pixel in Y direction
        gl.pixelStorei( gl.UNPACK_FLIP_Y_WEBGL, true );
        gl.texImage2D( gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, _texture.image );
        
        //set up parameteres
        gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST );
        gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST );
        gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE );
        gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE );
        gl.bindTexture( gl.TEXTURE_2D, null );
    };

    /*****************************************
     *          return the texture
     ***************************************/
    this.getTexture = function( )
    {
        return _texture;
    };

    /*****************************************
     *          enable texture mapping
     ***************************************/
    this.enable = function( )
    {
        var gl = Opengl.gl;
        gl.activeTexture( gl.TEXTURE0 + _textureId);
        gl.bindTexture( gl.TEXTURE_2D, _texture );
        if(_textureId == 0)
        	gl.uniform1i( ShaderControl.shaderProgram.vTexture0Uniform, _textureId );
        else
        	gl.uniform1i( ShaderControl.shaderProgram.vTexture1Uniform, _textureId );
        gl.enableVertexAttribArray(ShaderControl.shaderProgram.textureCoordAttribute);
    };
    
    /*****************************************
     *          disable texture
     ***************************************/
    this.disable = function( )
    {
        gl.disableVertexAttribArray(ShaderControl.shaderProgram.textureCoordAttribute);
    };

}
