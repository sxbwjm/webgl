                            CS870 Final Project (WebGL)
                                Final submission
                                   Xiaobo Sun
Quick Way to run:

Use Firefox to open index.html. 
If this does not work, try the following directions:
                                   
1. Prerequisites:
  1) WebGL-supported Web broswer
    .Google Chrome (Supported starting with 9.x; enabled by default)
    .Firefox (disabled by defalut)
        To enable, type "about:config" into the address bar, search for 
        "webgl", and double-click "webgl.force-enabled" to set it to true
        
  2) python ( for running a simple web sever) or other web server tool.
  
2. How to run:
  1) Go to command line, and move to the program source directory.
  2) run the following command to get a http server:
  
     python -m SimpleHTTPServer 8000
     
  3) Open Webgl-enabled web browser, and type "http://localhost:8000/"
     in address bar.
  4) Wait for Scenes to be loaded.
  5) Kill the SimpleHTTPServer after grading.

3. What's included in Beta submission
  1) Web UI                            -- 100%
  2) Wire & Solid sweep surface scenes -- 100%
  3) Lighting & Color scenes           -- 100%
  4) Texture                           -- 100%
  5) Multi-Texuture		       —- 100%
  

* Webgl does not work on agate for some reason.
* Texture does not work well in Chrome if open the index.html directly without running
  web server
  
