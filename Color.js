/**
 * Color class
 * @author Xiaobo Sun
 * Created: 11/18/2013
 */
function Color(r, g, b)
{
	this.r = r;
	this.g = g;
	this.b = b;
	

	/*****************************************
	*            get array of color
	***************************************/
	this.getArray = function()
	{
		var out = new Array(3);
		out[0] = this.r;
		out[1] = this.g;
		out[2] = this.b;
		
		return out;
	};
	
	/*****************************************
	*            set values for color
	***************************************/
	this.setValues = function(r, g, b)
	{
		this.r = r;
		this.g = g;
		this.b = b;
	};
	
}

/*****************************************
*            pre-defined colors
***************************************/
Color.red = new Color(1, 0, 0);
Color.green = new Color(0, 1, 0);
Color.blue = new Color(0, 0, 1);
Color.black = new Color(0, 0, 0);
Color.white = new Color(1, 1, 1);
