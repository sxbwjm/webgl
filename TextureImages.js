/**
 * TextureControl: singleton pattern
 * 
 * Manage textures
 */

var TextureImages = new function()
{
    var _images = new Array();
    var _imgCount = 0;
    var _imgNumber = 0;
    var _callback = null;
    
    this.loadImages = function(callback)
    {
        _callback = callback;
        
        addImage("background.jpg");
        addImage("dog.jpg");
        addImage("dog_no_bg.jpg");
        addImage("doraemon.jpg");
        addImage("foreground.jpg");
        addImage("title.jpg");
    };
    
    this.getImage = function(name)
    {
        return _images[name];
    };
    
    function addImage(img)
    {
        var image = new Image();
        
        _images[img] = image;
        _imgNumber++;
        image.onload = imgLoaded;
        image.src = img;

    };
    
    function imgLoaded()
    {
        _imgCount++;
        if(_imgCount == _imgNumber)
        {
            _callback();
        }
    };
};
