/**
 * Point3D class: save 3D vector
 * @author Xiaobo Sun
 * created: 11/20/2013
 */
function Point3D(x, y, z)
{
	this.x = x;
	this.y = y;
	this.z = z;
	
	/*****************************************
	*   get array of the point
	***************************************/
	this.getArray = function()
	{
		var out = new Array(3);
		out[0] = this.x;
		out[1] = this.y;
		out[2] = this.z;
		
		return out;
	};
	
	/*****************************************
	*   set values for the point
	***************************************/
	this.setValues = function(x, y, z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	};
	
	/*****************************************
    *   get vec3
    ***************************************/
	this.getVec3 = function()
	{
	    var vec = vec3.create([this.x, this.y, this.z]);
	    return vec;
	};
	
	this.toString = function()
	{
	    return "[" + this.x + ", " + this.y + ", " + this.z + "]";
	};
}
