/**
 * LightColor: manage ambient, diffuse and specular light color
 * 
 * @author Xiaobo Sun
 * @version 1.0 10/26/2013
 * 
 */
function LightColor()
{
    var _lightAmbient = [0.2, 0.2, 1.0, 1.0];
    var _lightDiffuse = [0.8, 0.8, 0.8, 1.0];
    var _lightSpecular = [0.0, 0.0, 0.0, 1.0];

    /**
     * set ambient values
     */
    this.setAmbient = function( r,  g, b, a )
    {
        _lightAmbient[0] = r;
        _lightAmbient[1] = g;
        _lightAmbient[2] = b;
        _lightAmbient[3] = a;
    };
    
    this.getAmbient = function()
    {
        return _lightAmbient;
    };

    /**
     * set ambient red value
     */
    this.setAmbientR = function( value )
    {
        _lightAmbient[0] = value;
    };

    /**
     * set ambient green
     */
    this.setAmbientG = function( value )
    {
        _lightAmbient[1] = value;
    };

    /**
     * set ambient blue
     */
    this.setAmbientB = function( value )
    {
        _lightAmbient[2] = value;
    };

    /**
     * set ambient alpha
     */
    this.setAmbientA = function( value )
    {
        _lightAmbient[3] = value;
    };

    /**
     * set diffuse values
     */
    this.setDiffuse = function( r, g, b, a )
    {
        _lightDiffuse[0] = r;
        _lightDiffuse[1] = g;
        _lightDiffuse[2] = b;
        _lightDiffuse[3] = a;
    };

    /**
     * get diffuse values
     */
    this.getDiffuse = function()
    {
        return _lightDiffuse;
    };
    
    /**
     * set diffuse red
     */
    this.setDiffuseR = function( value )
    {
        _lightDiffuse[0] = value;
    };

    /**
     * set diffuse green
     * 
     * @param value
     */
    this.setDiffuseG = function( value )
    {
        _lightDiffuse[1] = value;
    };

    /**
     * set diffuse blue
     */
    this.setDiffuseB = function( value )
    {
        _lightDiffuse[2] = value;
    };

    /**
     * set diffuse alpha
     * 
     * @param value
     */
    this.setDiffuseA = function( value )
    {
        _lightDiffuse[3] = value;
    };

    /**
     * set specular values
     */
    this.setSpecular = function( r, g, b, a)
    {
        _lightSpecular[0] = r;
        _lightSpecular[1] = g;
        _lightSpecular[2] = b;
        _lightSpecular[3] = a;
    };
    
     /**
     * get specular values
     */
    this.getSpecular = function()
    {
        return _lightSpecular;
    };

    /**
     * set specular red
     */
    this.setSpecularR = function( value )
    {
        _lightSpecular[0] = value;
    };

    /**
     * set specular green
     */
    this.setSpecularG = function( value )
    {
        _lightSpecular[1] = value;
    };

    /**
     * set specular blue
     */
    this.setSpecularB = function( value )
    {
        _lightSpecular[2] = value;
    };

    /**
     * set specluar alpha
     */
    this.setSpecularA = function( value )
    {
        _lightSpecular[3] = value;
    };
}
