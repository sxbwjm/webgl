/**
 * CommonSliders: 3 sliders shared by lights, tranforms, etc., to adjust their
 * parameters
 * 
 * @author xiaobo
 * @version 1.0 11/30/2013
 * 
 */
var CommonSliders = new function()
{
    var FACTOR = 100;
    var _adjustable   = null;

    /**
     * specify the object to be adjusted
     * @param adjustable
     */
    this.setAdjustable = function( adjustable )
    {
        _adjustable = adjustable;
        var slider1 = getSlider(1);
        var slider2 = getSlider(2);
        var slider3 = getSlider(3);
        
        var value1 = ( _adjustable.getValue1() * FACTOR );
        var value2 = ( _adjustable.getValue2() * FACTOR );
        var value3 = ( _adjustable.getValue3() * FACTOR );

        slider1.min = ( _adjustable.getMinValue() * FACTOR );
        slider2.min = ( _adjustable.getMinValue() * FACTOR );
        slider3.min = ( _adjustable.getMinValue() * FACTOR );

        slider1.max = ( _adjustable.getMaxValue() * FACTOR );
        slider2.max = ( _adjustable.getMaxValue() * FACTOR );
        slider3.max = ( _adjustable.getMaxValue() * FACTOR );

        slider1.value = value1;
        slider2.value = value2;
        slider3.value = value3;

        setAxisName();
    };

    
    function getSlider(id)
    {
        return document.getElementById("slider" + id);
    }
    
    function setAxisName( )
    {
         var axisLabel1 = document.getElementById("slider_axis_label1");
         var axisLabel2 = document.getElementById("slider_axis_label2");
         var axisLabel3 = document.getElementById("slider_axis_label3");
         
         axisLabel1.innerHTML = _adjustable.getAxisName1();
         axisLabel2.innerHTML = _adjustable.getAxisName2();
         axisLabel3.innerHTML = _adjustable.getAxisName3();
         
    };

    /**
     * SliderListener
     */
    this.onSliderChange = function(id)
    {
        if ( _adjustable == null )
        {
            return;
        }
        var value = getSlider(id).value / FACTOR;
        switch(id)
        {
            case 1:
                _adjustable.setValue1( value );
                break;
            case 2:
                _adjustable.setValue2( value );
                break;
            case 3:
                _adjustable.setValue3( value );
                break;
        }
        SceneControl.curScene().redraw();
        ControlPanel.updateParamStatus();
            
     };
    
};
