/*
 * SliderAdjustable: interface for adjusting values by common sliders
 */
function SliderAdjustable( dataArray, min, max, axisName )
{
    var _dataArray = dataArray;
    var _min = min;
    var _max = max;
    var _axis1 = "X";
    var _axis2 = "Y";
    var _axis3 = "Z";
    
    if(axisName && axisName.length == 3)
    {
        _axis1 = axisName.charAt(0);
        _axis2 = axisName.charAt(1);
        _axis3 = axisName.charAt(2);
    }
    
    this.setValue1 = function( value )
    {
        _dataArray[0] = value;
    };
    this.setValue2 = function( value )
    {
        _dataArray[1] = value;
    };
    this.setValue3 = function( value )
    {
        _dataArray[2] = value;
    };
    this.getValue1 = function( )
    {
        return _dataArray[0];
    };
    this.getValue2 = function( )
    {
        return _dataArray[1];
    };
    this.getValue3 = function( )
    {
        return _dataArray[2];
    };
    this.getMaxValue = function( )
    {
        return _max;
    };
    this.getMinValue = function( )
    {
        return _min;
    };
    this.getAxisName1 = function()
    {
        return _axis1;
    };
    this.getAxisName2 = function()
    {
        return _axis2;
    };
    this.getAxisName3 = function()
    {
        return _axis3;
    };
}
