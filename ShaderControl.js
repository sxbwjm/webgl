/*
 * ShaderControl class
 * @author Xiaobo Sun
 * Created: 11/18/2013
 */
ShaderControl = new function()
{
	this.shaderProgram = null;

	function getShader(fileName, shaderType)
	{
		var gl = Opengl.gl;
	    var shader;
	    var code = null;
	    // get shader source synchronously
	     $.ajax({
					url: fileName,
					type: 'get',
					dataType: 'text',
					async: false,
					success: function(data) 
					{
	    				code = data;
					} 
				});
	
		shader = gl.createShader(shaderType);
	
		gl.shaderSource(shader, code);
		gl.compileShader(shader);
	
		if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
		{
			alert(gl.getShaderInfoLog(shader));
			return null;
		}
	
		return shader;
	}

	//function initShaders() 
	this.initShaders = function()
	{
		var gl = Opengl.gl;
		
		var vertexShader = getShader("shader.vs", gl.VERTEX_SHADER);
		var fragmentShader = getShader("shader.fs", gl.FRAGMENT_SHADER);
	
		this.shaderProgram = gl.createProgram();
		gl.attachShader(this.shaderProgram, vertexShader);
		gl.attachShader(this.shaderProgram, fragmentShader);
		gl.linkProgram(this.shaderProgram);
	
		if (!gl.getProgramParameter(this.shaderProgram, gl.LINK_STATUS)) {
			alert("Could not initialise shaders");
		}
	
		gl.useProgram(this.shaderProgram);
	
	    // vertex array attribute
		this.shaderProgram.vertexPositionAttribute = gl.getAttribLocation(this.shaderProgram, "aVertexPosition");
		gl.enableVertexAttribArray(this.shaderProgram.vertexPositionAttribute);
		
		// texture array attribute
		this.shaderProgram.textureCoordAttribute = gl.getAttribLocation(this.shaderProgram, "aTextureCoord");
		//gl.enableVertexAttribArray(this.shaderProgram.textureCoordAttribute);
	    this.shaderProgram.vertexNormalAttribute = gl.getAttribLocation(this.shaderProgram, "aVertexNormal");
	    gl.enableVertexAttribArray(this.shaderProgram.vertexNormalAttribute);
		
	    // color
		this.shaderProgram.vColorUniform = gl.getUniformLocation(this.shaderProgram, "vColor");
	    // texture
		//this.shaderProgram.vTexture0Uniform = gl.getUniformLocation(this.shaderProgram, "uTexture0");
		//this.shaderProgram.vTexture1Uniform = gl.getUniformLocation(this.shaderProgram, "uTexture1");
		this.shaderProgram.textureIdsUniform = gl.getUniformLocation(this.shaderProgram, "uTextureIds");
		this.shaderProgram.textureAlphasUniform = gl.getUniformLocation(this.shaderProgram, "uTextureAlphas");
		this.shaderProgram.textureOnOffsUniform = gl.getUniformLocation(this.shaderProgram, "uTextureOnOffs");
	    this.shaderProgram.textureNumUniform = gl.getUniformLocation(this.shaderProgram, "uTextureNum");
	    this.shaderProgram.useTexture = gl.getUniformLocation(this.shaderProgram, "uUseTexture");
		// pmv matrix
	    this.shaderProgram.pMatrixUniform = gl.getUniformLocation(this.shaderProgram, "uPMatrix");
		this.shaderProgram.mvMatrixUniform = gl.getUniformLocation(this.shaderProgram, "uMVMatrix");
		this.shaderProgram.nMatrixUniform = gl.getUniformLocation(this.shaderProgram, "uNMatrix");
		// light0
		this.shaderProgram.useLightingUniform = gl.getUniformLocation(this.shaderProgram, "uUseLighting");
        this.shaderProgram.light0EnableUniform = gl.getUniformLocation(this.shaderProgram, "uLight0Enabled");
        this.shaderProgram.light0PosUniform = gl.getUniformLocation(this.shaderProgram, "uLight0Pos");
        this.shaderProgram.light0AmbientUniform = gl.getUniformLocation(this.shaderProgram, "uLight0Ambient");
        this.shaderProgram.light0DiffuseUniform = gl.getUniformLocation(this.shaderProgram, "uLight0Diffuse");
        this.shaderProgram.light0SpecularUniform = gl.getUniformLocation(this.shaderProgram, "uLight0Specular");
		// light1
		this.shaderProgram.light1EnableUniform = gl.getUniformLocation(this.shaderProgram, "uLight1Enabled");
        this.shaderProgram.light1PosUniform = gl.getUniformLocation(this.shaderProgram, "uLight1Pos");
        this.shaderProgram.light1AmbientUniform = gl.getUniformLocation(this.shaderProgram, "uLight1Ambient");
        this.shaderProgram.light1DiffuseUniform = gl.getUniformLocation(this.shaderProgram, "uLight1Diffuse");
        this.shaderProgram.light1SpecularUniform = gl.getUniformLocation(this.shaderProgram, "uLight1Specular");
        
        this.shaderProgram.eyePosUniform = gl.getUniformLocation(this.shaderProgram, "uEyePos");
		//return shaderProgram;
	};
};