/**
 * Cube: basic class for all 3D shapes
 *
 * @author Xiaobo Sun
 */
function Cube( )
{
    // inherit from Object3D class
    Object3D.call( this );

    var _vertices = [
    //front
    [ -1, -1, 1, 1, -1, 1, -1, 1, 1, 1, 1, 1 ],

    //back
    [ 1, -1, -1, -1, -1, -1, 1, 1, -1, -1, 1, -1 ],
    //top
    [ -1, 1, 1, 1, 1, 1, -1, 1, -1, 1, 1, -1 ],

    //bottom
    [ 1, -1, 1, -1, -1, 1, 1, -1, -1, -1, -1, -1 ],

    //left
    [ -1, -1, -1, -1, -1, 1, -1, 1, -1, -1, 1, 1 ],

    //right
    //[ 1, -1, 1, 1, -1, -1, 1, 1, 1, 1, -1, -1 ] 
    [1, -1, -1, 1, 1, -1, 1, 1, 1, 1, -1, 1]
    ];

    var _normals = [
    //front
    [ 0.0,  0.0,  1.0,  0.0,  0.0,  1.0,  0.0,  0.0,  1.0,  0.0,  0.0,  1.0],
    //back
    [ 0.0,  0.0, -1.0,  0.0,  0.0, -1.0,  0.0,  0.0, -1.0,  0.0,  0.0, -1.0],
    //top
    [ 0.0,  1.0,  0.0,  0.0,  1.0,  0.0,  0.0,  1.0,  0.0,  0.0,  1.0,  0.0],
    //bottom
    [ 0.0, -1.0,  0.0,  0.0, -1.0,  0.0,  0.0, -1.0,  0.0,  0.0, -1.0,  0.0],
    //left
    [-1.0,  0.0,  0.0, -1.0,  0.0,  0.0, -1.0,  0.0,  0.0, -1.0,  0.0,  0.0],
    //right
    [ 1.0,  0.0,  0.0,  1.0,  0.0,  0.0,  1.0,  0.0,  0.0,  1.0,  0.0,  0.0]
    ];
    var _vertexPositionBuffer = [ ];
    var _vertexNormalBuffer = [];

    /*****************************************
     *   initialize array buffer of vertices
     ***************************************/
    this.init = function( )
    {
        var gl = Opengl.gl;
        for ( var i = 0; i < _vertices.length; i++ )
        {
            _vertexPositionBuffer[ i ] = gl.createBuffer( );
            gl.bindBuffer( gl.ARRAY_BUFFER, _vertexPositionBuffer[ i ] );

            gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( _vertices[ i ] ), gl.STATIC_DRAW );
            _vertexPositionBuffer[ i ].itemSize = 3;
            _vertexPositionBuffer[ i ].numItems = 4;
            
            _vertexNormalBuffer[i] = gl.createBuffer( );
            gl.bindBuffer( gl.ARRAY_BUFFER, _vertexNormalBuffer[ i ]);
            gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( _normals[ i ] ), gl.STATIC_DRAW );
            _vertexNormalBuffer[ i ].itemSize = 3;
            _vertexNormalBuffer[ i ].numItems = 4;
        }

    };

    /*****************************************
     *               draw cube
     ***************************************/
    this.draw = function( )
    {
        var gl = Opengl.gl;
        for ( var i = 0; i < _vertices.length; i++ )
        {
            // vertex buffer
            gl.bindBuffer( gl.ARRAY_BUFFER, _vertexPositionBuffer[ i ] );
            gl.vertexAttribPointer( ShaderControl.shaderProgram.vertexPositionAttribute, _vertexPositionBuffer[ i ].itemSize, gl.FLOAT, false, 0, 0 );
            // normal buffer
            gl.bindBuffer( gl.ARRAY_BUFFER, _vertexNormalBuffer[ i ] );
            gl.vertexAttribPointer( ShaderControl.shaderProgram.vertexNormalAttribute, _vertexNormalBuffer[ i ].itemSize, gl.FLOAT, false, 0, 0 );
            
            gl.drawArrays( gl.TRIANGLE_STRIP, 0, _vertexPositionBuffer[ i ].numItems );
        }
    };

}
