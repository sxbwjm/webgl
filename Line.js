/**
 * Line : draw a line
 * 
 * @author Xiaobo Sun
 */
function Line(vertices, normals)
{
	// inherit from Object3D class
    Object3D.call(this);

	var _vertices = vertices;
	var _normals = normals;
	var _vertexPositionBuffer;
	var _vertexNormalBuffer;
	
	 /*****************************************
	*   initialize array buffer of vertices
	***************************************/
	this.init = function()
	{
    	var gl = Opengl.gl;
        _vertexPositionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, _vertexPositionBuffer);     
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(_vertices), gl.STATIC_DRAW);
        _vertexPositionBuffer.itemSize = 3;
        _vertexPositionBuffer.numItems = _vertices.length / 3;
        
        _vertexNormalBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, _vertexNormalBuffer);     
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(_normals), gl.STATIC_DRAW);
        _vertexNormalBuffer.itemSize = 3;
        _vertexNormalBuffer.numItems = _normals.length / 3;

   };
   
   /*****************************************
	*          draw triangle
	***************************************/
   this.draw = function()
   {
   		var gl = Opengl.gl;
        gl.lineWidth(3);
        gl.bindBuffer(gl.ARRAY_BUFFER, _vertexPositionBuffer);
        gl.vertexAttribPointer(ShaderControl.shaderProgram.vertexPositionAttribute, _vertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, _vertexNormalBuffer);
        gl.vertexAttribPointer(ShaderControl.shaderProgram.vertexNormalAttribute, _vertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);
        
        gl.drawArrays(gl.LINES, 0, _vertexPositionBuffer.numItems);
   }