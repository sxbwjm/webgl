 /**
  * TextureControl class: Basic opengl operation
  * @author Xiaobo
  * Created: 12/08/2013
  */
function TextureControl()
{
	var _textureList = [];
	var _textureIdList = [];
	var _textureAlphaList = [];
	var _textureOnOffList = [];
	
	/*****************************************
     *          add texture
     ***************************************/
	this.addTexture = function(img)
	{
		var num = _textureList.length;
		_textureList.push(new Texture(img, num));
		_textureIdList.push(num);
		_textureAlphaList.push(0.5);
		_textureOnOffList.push(true);
	};
	
	/*****************************************
     *          get texture
     ***************************************/
	this.getTexture = function(id)
	{
		return _textureList[id];
	};
	
	/*****************************************
     *          get texture
     ***************************************/
	this.setOnOff = function(id, onOff)
	{
		return _textureOnOffList[id] = onOff;
	};
	
	/*****************************************
     *          initialize all textures
     ***************************************/
	this.init = function()
	{
		for(var i=0; i<_textureList.length; i++)
		{
			_textureList[i].init();
		}
	};
	
	/*****************************************
     *     update alpha value of texture
     ***************************************/
	this.updateAlpha = function(id, value)
	{
		_textureAlphaList[id] = value;
	};
	
	/*****************************************
     *          enable all textures
     ***************************************/
	this.enable = function()
	{
		for(var i=0; i<_textureList.length; i++)
		{
			_textureList[i].enable();
		}
		
		if(_textureList.length > 0)
		{
			gl.uniform1i( ShaderControl.shaderProgram.useTexture, true );
			gl.uniform1iv( ShaderControl.shaderProgram.textureIdsUniform, _textureIdList );
			gl.uniform1fv( ShaderControl.shaderProgram.textureAlphasUniform, _textureAlphaList );
			gl.uniform1iv( ShaderControl.shaderProgram.textureOnOffsUniform, _textureOnOffList );
		}
		
		gl.uniform1i( ShaderControl.shaderProgram.textureNumUniform, _textureList.length );
	};
	
	/*****************************************
     *          disable all textures
     ***************************************/
	this.disable = function()
	{
		for(var i=0; i<_textureList.lenght; i++)
		{
			_textureList[i].disable();
		}
		
		gl.uniform1i( ShaderControl.shaderProgram.useTexture, false );
	};
}


/*****************************************
 *    update alpha value of texture
 *         (static method)
 ***************************************/
TextureControl.updateTextureAlpha = function(id, value)
{
	var curScene = SceneControl.curScene();
	var curTextures = curScene.getActiveObject().getTextures();
	curTextures.updateAlpha(id, value * 1.0 );
	
	curScene.redraw();
	
};

/*****************************************
 *    set on/off of texture
 *         (static method)
 ***************************************/
TextureControl.setTextureOnOff = function(id, onOff)
{
	var curScene = SceneControl.curScene();
	var curTextures = curScene.getActiveObject().getTextures();
	curTextures.setOnOff(id, onOff);
	
	curScene.redraw();
	
};