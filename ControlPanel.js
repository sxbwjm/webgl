/**
 * ControlPanel: Manage the control panel
 * 
 * Singleton pattern
 *
 * @author Xiaobo Sun
 */

var ControlPanel = new function()
{
    var _curScene = null;
    
    /*****************************************
    *         move to a new scene
    ***************************************/
    function moveToNewScene(scene)
    {
        var txtbox_sceneName = document.getElementById("scene_name");
        txtbox_sceneName.value = scene.getSceneName();
        
        scene.init();
        
        // upate parameter status
        updateParamStatus();
        
        // select light0 position as default parameter
        var radio_light0Pos = document.getElementById("light0_pos");
        radio_light0Pos.click();
        var light = scene.getLights().getLight(0);
        CommonSliders.setAdjustable(light.sliderAdjustablePos);
        
        var multiTexturePanelDisp = document.getElementById("multi_texture_panel").style.display;
        if(scene.getSceneMode() == Scene.MODE_MULTI_TEXTURE)
        {
        	$("#multi_texture_panel").show();
       	}
        else
        {
        	$("#multi_texture_panel").hide();
        }

        scene.redraw();
    }
    
    /*****************************************
    *         next scene
    ***************************************/
    this.onNextScene = function()
    {
        _curScene =  SceneControl.nextScene();
        moveToNewScene(_curScene);
    };
    
    /*****************************************
    *         previous scene
    ***************************************/
    this.onPreScene = function()
    {
        _curScene = SceneControl.preScene();
        moveToNewScene(_curScene);
    };
    
    /*****************************************
    *         use light
    ***************************************/
    this.onUseLight = function(checkbox, id)
    {
        _curScene.getLights().getLight(id).setEnable(checkbox.checked);
        _curScene.redraw();
    };
    
    /*****************************************
    *     select parameter (radio button clicked)
    ***************************************/
    this.onSelectParam = function(radiobutton, id)
    {
        if(radiobutton.checked)
        {
            var light0 = _curScene.getLights().getLight(0);
            var light1 = _curScene.getLights().getLight(1);
            switch(radiobutton.id)
            {
                case "light0_pos":
                    CommonSliders.setAdjustable(light0.sliderAdjustablePos);
                    break;
                case "light0_ambient":
                    CommonSliders.setAdjustable(light0.sliderAdjustableAmbient);
                    break;
                case "light0_diffuse":
                    CommonSliders.setAdjustable(light0.sliderAdjustableDiffuse);
                    break;
                case "light0_specular":
                    CommonSliders.setAdjustable(light0.sliderAdjustableSpecular);
                    break;
               
                case "light1_pos":
                    CommonSliders.setAdjustable(light1.sliderAdjustablePos);
                    break;
                case "light1_ambient":
                    CommonSliders.setAdjustable(light1.sliderAdjustableAmbient);
                    break;
                case "light1_diffuse":
                    CommonSliders.setAdjustable(light1.sliderAdjustableDiffuse);
                    break;
                case "light1_specular":
                    CommonSliders.setAdjustable(light1.sliderAdjustableSpecular);
                    break;
            }
        }
    };
    
   this.updateParamStatus = function()
   {
       updateParamStatus();
   };
    
    function updateParamStatus()
    {
        var light0 = _curScene.getLights().getLight(0);
        var light1 = _curScene.getLights().getLight(1);
        
        var checkbox = document.getElementById("light0");
        checkbox.checked = light0.isEnabled();
        
        checkbox = document.getElementById("light1");
        checkbox.checked = light1.isEnabled();
        
        checkbox = document.getElementById("draw_axis");
        checkbox.checked = _curScene.isDrawAxis();
        
        checkbox = document.getElementById("draw_normal");
        checkbox.checked = _curScene.isDrawNormals();
        
        checkbox = document.getElementById("draw_light");
        checkbox.checked = _curScene.isDrawLight();
        var label, data;
        
        label = document.getElementById("param_light0_pos");
        data = light0.getPos();
        label.innerHTML = paramToString(data);
        
        label = document.getElementById("param_light0_ambient");
        data = light0.getAmbient();
        label.innerHTML = paramToString(data);
        
        label = document.getElementById("param_light0_diffuse");
        data = light0.getDiffuse();
        label.innerHTML = paramToString(data);
        
        label = document.getElementById("param_light0_specular");
        data = light0.getSpecular();
        label.innerHTML = paramToString(data);
        
        label = document.getElementById("param_light1_pos");
        data = light1.getPos();
        label.innerHTML = paramToString(data);
        
        label = document.getElementById("param_light1_ambient");
        data = light1.getAmbient();
        label.innerHTML = paramToString(data);
        
        label = document.getElementById("param_light1_diffuse");
        data = light1.getDiffuse();
        label.innerHTML = paramToString(data);
        
        label = document.getElementById("param_light1_specular");
        data = light1.getSpecular();
        label.innerHTML = paramToString(data);
    };
    
    function paramToString(arr)
    {
        return "(" + 
                arr[0].toFixed(1) + ", " +
                arr[1].toFixed(1) + ", " +
                arr[2].toFixed(1) +
                ")";
    }
    
    /*****************************************
    *         draw axis
    ***************************************/
    this.onDrawAxis = function(checkbox)
    {
        _curScene.setDrawAxis(checkbox.checked);
        _curScene.redraw();
    };
    
    /*****************************************
    *         draw normals
    ***************************************/
    this.onDrawNormal = function(checkbox)
    {
        _curScene.setDrawNormals(checkbox.checked);
        _curScene.redraw();
    };
    
    /*****************************************
     *         draw light
     ***************************************/
     this.onDrawLight = function(checkbox)
     {
         _curScene.setDrawLight(checkbox.checked);
         _curScene.redraw();
     };
};

