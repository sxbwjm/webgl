/**
 * Object3D: basic class for all 3D shapes
 * 
 * @author Xiaobo Sun
 */
function Object3D()
{
	//tranformation data
	var _loc = new Point3D(0, 0, 0);
	var _size = new Point3D(1, 1, 1);
	var _color = new Color(0, 0, 0);
	var _alpha = 1;
	var _rotateVec = new Point3D(0, 0, 0);
	var _rotateAngle = 0.0;
	
	function dump()
	{
	    str = "loc:" + _loc.toString();
	    str += " size:" + _size.toString();
	    str += "rotate:" + _rotateAngle + _rotateVec.toString();
	    
	    Canvas.print(str);
	};
	
	/*****************************************
	 *            setSize
	 ****************************************/
	this.setLocation = function(x, y, z)
	{
		_loc.setValues(x, y, z);
	};
	
	/*****************************************
	 *            setSize
	 ****************************************/
	this.setSize = function(x, y, z)
	{
		_size.setValues(x, y, z);
	};
	
	/*****************************************
	 *            setColor
	 ****************************************/
	this.setColor = function(c)
	{
		_color.setValues(c.r, c.g, c.b);
	};
	
	/*****************************************
	 *            setRotate
	 ****************************************/
	this.setRotate = function(a, x, y, z)
	{
		_rotateVec.setValues(x, y, z);
		_rotateAngle = degToRad(a);
		//alert("deg:" + a + "rad:" + _rotateAngle);
	};
	
	/*****************************************
	 *            redraw
	 *  1. setup transformation
	 *  2. setup color
	 *  3. call the draw method (overridden by the actual shape)
	 ****************************************/
	this.redraw = function()
	{
		// setup transformation matrices
		Opengl.mvPushMatrix();
        mat4.translate(Opengl.mvMatrix, _loc.getArray());
        mat4.rotate(Opengl.mvMatrix, _rotateAngle, _rotateVec.getArray());
        mat4.scale(Opengl.mvMatrix, _size.getArray());
       	Opengl.setMatrixUniforms();
       	
       	// setup color
        Opengl.setColorUniforms([_color.r, _color.g, _color.b, _alpha]);
       //dump();
       	this.draw();
       	
       	//restore matrices
       	Opengl.mvPopMatrix();
        Opengl.setMatrixUniforms();
	};
	
	/*****************************************
	 *            draw
	 *  do the actual drawing job
	 *  (which will be overridden by the actual shape)
	 ****************************************/
	this.draw = function()
	{
		
	};
	
	/*****************************************
     *            init
     *  init the object (prepare to be drawed)
     *  (which will be overridden by the actual shape)
     ****************************************/
	this.init = function()
	{
	    
	};
	
	function degToRad(deg)
	{
	    
	    return (deg * 1.0 / 180) * Math.PI; 
	};
} 