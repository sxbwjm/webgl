/**
 * Light class: control light
 * @author Xiaobo Sun
 * created: 11/23/2013
 */

function Light( id )
{
    // inherit from LightColor
    LightColor.call( this );

    var _lightId = id;
    var _isEnabled = true;
    var _lightPos = [ 0, 0, 1, 0 ];

    /**
     * set light as a point light
     */
    this.setPointLight = function( )
    {
        _lightPos[ 3 ] = 1;
    };

    /**
     * set light as directional light
     */
    this.setDirectionLight = function( )
    {
        _lightPos[ 3 ] = 0;
    };

    /**
     * set position or direction
     */
    this.setPos = function( x, y, z )
    {
        _lightPos[ 0 ] = x;
        _lightPos[ 1 ] = y;
        _lightPos[ 2 ] = z;
    };
    
    /**
     * get position or direction
     */
    this.getPos = function()
    {
        return _lightPos;
    };

    /**
     * set position or direction X
     */
    this.setPosX = function( value )
    {
        _lightPos[ 0 ] = value;
    };

    /**
     * set position or direction Y
     */
    this.setPosY = function( value )
    {
        _lightPos[ 1 ] = value;
    };

    /**
     * set position or direction Z
     */
    this.setPosZ = function( value )
    {
        _lightPos[ 2 ] = value;
    };

    /**
     * enable light
     */
    this.setEnable = function( value )
    {
        _isEnabled = value;
    };

    /**
     * get status of light
     */
    this.isEnabled = function( )
    {
        return _isEnabled;
    };

    /**
     * apply light effect
     */
    this.update = function( )
    {
        var gl = Opengl.gl;
        
        // enalbe light
        if ( _isEnabled )
        {
            if ( _lightId == 0 )
            {
                gl.uniform1i( ShaderControl.shaderProgram.light0EnableUniform, true );
                gl.uniform4fv( ShaderControl.shaderProgram.light0PosUniform, _lightPos );
                gl.uniform4fv( ShaderControl.shaderProgram.light0AmbientUniform, this.getAmbient( ) );
                gl.uniform4fv( ShaderControl.shaderProgram.light0DiffuseUniform, this.getDiffuse( ) );
                gl.uniform4fv( ShaderControl.shaderProgram.light0SpecularUniform, this.getSpecular( ) );
            }
            else if ( _lightId == 1 )
            {
                gl.uniform1i( ShaderControl.shaderProgram.light1EnableUniform, true );
                gl.uniform4fv( ShaderControl.shaderProgram.light1PosUniform, _lightPos );
                gl.uniform4fv( ShaderControl.shaderProgram.light1AmbientUniform, this.getAmbient( ) );
                gl.uniform4fv( ShaderControl.shaderProgram.light1DiffuseUniform, this.getDiffuse( ) );
                gl.uniform4fv( ShaderControl.shaderProgram.light1SpecularUniform, this.getSpecular( ) );
            }
        }
        // disable light
        else
        {
            if ( _lightId == 0 )
            {
                gl.uniform1i( ShaderControl.shaderProgram.light0EnableUniform, false );
            }
            else if ( _lightId == 1 )
            {
                gl.uniform1i( ShaderControl.shaderProgram.light1EnableUniform, false );
            }
        }
    };
    if(_lightId == 0)
    {
    	this.sliderAdjustablePos = new SliderAdjustable( this.getPos( ), -1, 1 );
    }
    else
    {
    	this.sliderAdjustablePos = new SliderAdjustable( this.getPos( ), -10, 10 );
    }
    this.sliderAdjustableAmbient = new SliderAdjustable( this.getAmbient( ), 0, 1, "RGB" );
    this.sliderAdjustableDiffuse = new SliderAdjustable( this.getDiffuse( ), 0, 1, "RGB");
    this.sliderAdjustableSpecular = new SliderAdjustable( this.getSpecular( ), 0, 1, "RGB"); 
}

