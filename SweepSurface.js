/**
 * SweepSurface class: Create a Sweep surface by rotating a 2D curve
 *
 * @author Xiaobo Sun
 * Created: 11/20/2013
 */

function SweepSurface( xs, ys, slices )
{
    // inherit from Object3D class
    Object3D.call( this );

    var _xs = xs;
    var _ys = ys;
    var _vNum = 0;
    var _slices = slices;
    var _vertices = null;
    var _verticesU = null;
    var _verticesV = null;
    var _verticesNormal = null;
    
    var _curveVertexBuff = [ ];
    var _surfaceVertexBuff = [ ];
    var _textureBuff = [ ];
    var _curveVertexNormalBuff = [ ];
    var _surfaceVertexNormalBuff = [ ];
    var _normalLineBuff;
    
    var _isSolid = true;
    var _textures = null;
    
    var _isDrawNormals = false;
    
    // set the length of smaller array as vertices number
    if ( _xs.length > _ys.length )
    {
        _vNum = _ys.length;
    }
    else
    {
        _vNum = _xs.length;
    }

    // create vertices & uv coords
    createVertices( );
    createUVCoordinates();
    createNormals();


    /*****************************************
     *        setSolid
     *  set wire or solid 
     ****************************************/
    this.setSolid = function(value)
    {
        _isSolid = value;
    };
    
    /*****************************************
    *         set if draw normals
    ***************************************/
    this.setDrawNormals = function(value)
    {
        _isDrawNormals = value;
    };
    
     /*****************************************
     *        createVertices
     *  Create vertices based on curves
     ****************************************/
    function createVertices( )
    {
        _vertices = new Array( _slices + 1 );

        // total angle & slice angle
        var totalAngle = 2.0 * Math.PI;
        var da = totalAngle / _slices;

        for ( var s = 0; s < _slices + 1; s++ )
        {
            _vertices[ s ] = new Array( _vNum );

            var angle = s * da;

            if ( s == _slices )
            {
                angle = 0;
            }

            for ( var v = 0; v < _vNum; v++ )
            {
                _vertices[s][ v ] = new Point3D( );
                _vertices[s][ v ].x = _xs[ v ] * Math.cos( angle );
                _vertices[s][ v ].y = _ys[ v ];
                _vertices[s][ v ].z = - _xs[ v ] * Math.sin( angle );
            }
        }

        //dump( );
    };
    
     /**
     * create normals
     * 
     */
    function createNormals()
    {
        _verticesNormal = new Array( _slices + 1 );
        for ( var s = 0; s < _slices; s++ )
        {
            _verticesNormal[ s ] = new Array( _vNum );
            for ( var v = 0; v < _vNum; v++ )
            {
                var normal = vec3.create();

                //
                var p = _vertices[s][v];
                var p1, p2;

                // --------- caculate the sum of normals based on the triangles
                // --------- connected with privous curve
                
                // the index of previous curve
                var ps = s - 1;
                if ( ps == -1 )
                {
                    ps = _slices - 1;
                }
                
                // the first vertex of the curve
                if ( v == 0 )
                {
                    p1 = _vertices[ps][v + 1];
                    p2 = _vertices[ps][v];

                    normal = vec3.add(normal, getNormalizedNormal( p, p1, p2 ) );

                    p1 = _vertices[s][v + 1];
                    p2 = _vertices[ps][v + 1];
                    normal = vec3.add(normal, getNormalizedNormal( p, p1, p2 ) );
                }
                // the final vertex of the curve
                else if ( v == _vNum - 1 )
                {
                    p1 = _vertices[ps][v];
                    p2 = _vertices[s][v - 1];

                    normal = vec3.add(normal, getNormalizedNormal( p, p1, p2 ) );
                }
                // middle vertex of the curve
                else
                {
                    p1 = _vertices[ps][v];
                    p2 = _vertices[s][v - 1];
                    normal = vec3.add(normal, getNormalizedNormal( p, p1, p2 ) );

                    p1 = _vertices[ps][v + 1];
                    p2 = _vertices[ps][v];
                    normal = vec3.add(normal, getNormalizedNormal( p, p1, p2 ) );

                    p1 = _vertices[s][v + 1];
                    p2 = _vertices[ps][v + 1];
                    normal = vec3.add(normal, getNormalizedNormal( p, p1, p2 ) );
                }

                // --------- caculate the sum of normals based on the triangles
                // --------- connected with next curve

                var ns = s + 1;
                // the first vertex of the curve
                if ( v == 0 )
                {
                    p1 = _vertices[ns][v];
                    p2 = _vertices[s][v + 1];
                    normal = vec3.add(normal, getNormalizedNormal( p, p1, p2 ) );
                }
                // the final vertex of the curve
                else if ( v == _vNum - 1 )
                {
                    p1 = _vertices[s][v - 1];
                    p2 = _vertices[ns][v - 1];
                    normal = vec3.add(normal, getNormalizedNormal( p, p1, p2 ) );

                    p1 = _vertices[ns][v - 1];
                    p2 = _vertices[ns][v];
                    normal = vec3.add(normal, getNormalizedNormal( p, p1, p2 ) );
                }
                // middle vertex of the curve
                else
                {
                    p1 = _vertices[s][v - 1];
                    p2 = _vertices[ns][v - 1];
                    normal = vec3.add(normal, getNormalizedNormal( p, p1, p2 ) );

                    p1 = _vertices[ns][v - 1];
                    p2 = _vertices[ns][v];
                    normal = vec3.add(normal, getNormalizedNormal( p, p1, p2 ) );

                    p1 = _vertices[ns][v];
                    p2 = _vertices[s][v + 1];
                    normal = vec3.add(normal, getNormalizedNormal( p, p1, p2 ) );
                }
                var n = vec3.normalize(normal);
                var pNormal = new Point3D(n[0], n[1], n[2]);
                _verticesNormal[s][v] = pNormal;
                var debug = normal;
                Canvas.print("[" + s + "][" + v + "] " + debug[0] + "," + debug[1] + "," + debug[2] + "\n");
            }
        }

        // the last curve is same with the first curve
        _verticesNormal[ _slices ] = new Array( _vNum );
        for ( var v = 0; v < _vNum; v++ )
        {
            _verticesNormal[_slices][v] = _verticesNormal[0][v];
        }
    }
    
    function getNormalizedNormal( p, p1, p2 )
    {
        var v1 = vec3.subtract(p1.getVec3(), p.getVec3());
        var v2 = vec3.subtract(p2.getVec3(), p.getVec3());

        var n = vec3.cross(v1, v2);
        return vec3.normalize(n);
    }
    
    /**
     * create uv coordinates of object
     */
    function createUVCoordinates()
    {
        _verticesU = new Array(_slices + 1);
        _verticesV = new Array(_vNum);

        for ( var s = 0; s < _slices + 1; s++ )
        {
            _verticesU[s] = s * 1.0 / _slices;
        }

        // total length of curve
        var totalLen = 0;
        _verticesV[0] = 0;
        for ( var v = 1; v < _vNum; v++ )
        {
            totalLen += Math.sqrt( ( _xs[v] - _xs[v - 1] )
                    * ( _xs[v] - _xs[v - 1] ) + ( _ys[v] - _ys[v - 1] )
                    * ( _ys[v] - _ys[v - 1] ) );
            _verticesV[v] = totalLen;

        }

        // v values of uv coordinates
        for ( var v = 0; v < _vNum; v++ )
        {
            _verticesV[v] = _verticesV[v] * 1.0 / totalLen;
        }
    }

    

    /*****************************************
     *            createCurveVetexBuff
     *  Create vertex buffer for curves
     ****************************************/
    function createCurveVetexBuff( )
    {
        var gl = Opengl.gl;
        for ( var s = 0; s < _slices; s++ )
        {
            //
            var arr = [ ];

            for ( var v = 0; v < _vNum; v++ )
            {
                arr[ 3 * v ] = _vertices[s][ v ].x;
                arr[ 3 * v + 1 ] = _vertices[s][ v ].y;
                arr[ 3 * v + 2 ] = _vertices[s][ v ].z;
            }

            //
            _curveVertexBuff[ s ] = gl.createBuffer( );
            gl.bindBuffer( gl.ARRAY_BUFFER, _curveVertexBuff[ s ] );

            gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( arr ), gl.STATIC_DRAW );
            _curveVertexBuff[ s ].itemSize = 3;
            _curveVertexBuff[ s ].numItems = _vNum;
        }
    };
    
     /*****************************************
     *            createCurveVetexNormalBuff
     *  Create vertex normal buffer for curves
     ****************************************/
    function createCurveVetexNormalBuff( )
    {
        var gl = Opengl.gl;
        for ( var s = 0; s < _slices; s++ )
        {
            //
            var arr = [ ];

            for ( var v = 0; v < _vNum; v++ )
            {
                arr[ 3 * v ] = _verticesNormal[s][ v ].x;
                arr[ 3 * v + 1 ] = _verticesNormal[s][ v ].y;
                arr[ 3 * v + 2 ] = _verticesNormal[s][ v ].z;
            }

            //
            _curveVertexNormalBuff[ s ] = gl.createBuffer( );
            gl.bindBuffer( gl.ARRAY_BUFFER, _curveVertexNormalBuff[ s ] );

            gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( arr ), gl.STATIC_DRAW );
            _curveVertexNormalBuff[ s ].itemSize = 3;
            _curveVertexNormalBuff[ s ].numItems = _vNum;
        }
    };

     /*****************************************
     *        createSurfaceVetexBuff
     *  Create vertex buffer for surface
     ****************************************/
    function createSurfaceVetexBuff( )
    {
        var gl = Opengl.gl;
        for ( var s = 0; s < _slices; s++ )
        {
            //
            var arr = [ ];

            for ( var v = 0; v < _vNum; v++ )
            {
                arr[ 6 * v ] = _vertices[s][ v ].x;
                arr[ 6 * v + 1 ] = _vertices[s][ v ].y;
                arr[ 6 * v + 2 ] = _vertices[s][ v ].z;

                arr[ 6 * v + 3 ] = _vertices[s + 1][ v ].x;
                arr[ 6 * v + 4 ] = _vertices[s + 1][ v ].y;
                arr[ 6 * v + 5 ] = _vertices[s + 1][ v ].z;
            }

            //
            _surfaceVertexBuff[ s ] = gl.createBuffer( );
            gl.bindBuffer( gl.ARRAY_BUFFER, _surfaceVertexBuff[ s ] );

            gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( arr ), gl.STATIC_DRAW );
            _surfaceVertexBuff[ s ].itemSize = 3;
            _surfaceVertexBuff[ s ].numItems = 2 * _vNum;
        }
    }
    
     /*****************************************
     *        createSurfaceVetexNormalBuff
     *  Create vertex normal buffer for surface
     ****************************************/
    function createSurfaceVetexNormalBuff( )
    {
        var gl = Opengl.gl;
        for ( var s = 0; s < _slices; s++ )
        {
            //
            var arr = [ ];

            for ( var v = 0; v < _vNum; v++ )
            {
                arr[ 6 * v ] = _verticesNormal[s][ v ].x;
                arr[ 6 * v + 1 ] = _verticesNormal[s][ v ].y;
                arr[ 6 * v + 2 ] = _verticesNormal[s][ v ].z;

                arr[ 6 * v + 3 ] = _verticesNormal[s + 1][ v ].x;
                arr[ 6 * v + 4 ] = _verticesNormal[s + 1][ v ].y;
                arr[ 6 * v + 5 ] = _verticesNormal[s + 1][ v ].z;
            }

            //
            _surfaceVertexNormalBuff[ s ] = gl.createBuffer( );
            gl.bindBuffer( gl.ARRAY_BUFFER, _surfaceVertexNormalBuff[ s ] );

            gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( arr ), gl.STATIC_DRAW );
            _surfaceVertexNormalBuff[ s ].itemSize = 3;
            _surfaceVertexNormalBuff[ s ].numItems = 2 * _vNum;
        }
    }
    
     /*****************************************
     *        createNormalLineBuff
     *  Create vertex normal buffer for normal line
     ****************************************/
    function createNormalLineBuff( )
    {
        var gl = Opengl.gl;
        var arr = [ ];
        for ( var s = 0; s < _slices; s++ )
        {
            
            for ( var v = 0; v < _vNum; v++ )
            {
                var offset = s * _vNum + v;
                arr[ 6 * offset ] = _vertices[s][ v ].x;
                arr[ 6 * offset + 1 ] = _vertices[s][ v ].y;
                arr[ 6 * offset + 2 ] = _vertices[s][ v ].z;

                arr[ 6 * offset + 3 ] = _verticesNormal[s][ v ].x + _vertices[s][ v ].x;
                arr[ 6 * offset + 4 ] = _verticesNormal[s][ v ].y + _vertices[s][ v ].y;
                arr[ 6 * offset + 5 ] = _verticesNormal[s][ v ].z + _vertices[s][ v ].z;
            }

        }
        
        
        //
        _normalLineBuff = gl.createBuffer( );
        gl.bindBuffer( gl.ARRAY_BUFFER, _normalLineBuff);
        
        gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( arr ), gl.STATIC_DRAW );
        _normalLineBuff.itemSize = 3;
        _normalLineBuff.numItems = 2 * _slices * _vNum;
    }
    
    
    /*****************************************
     *        createTextureVetexBuff
     *  Create vertex buffer for surface
     ****************************************/
    function createTextureVetexBuff( )
    {
        var gl = Opengl.gl;
        for ( var s = 0; s < _slices; s++ )
        {
            //
            var arr = [ ];

            for ( var v = 0; v < _vNum; v++ )
            {
                arr[ 4 * v ] = _verticesU[s];       // next curve
                arr[ 4 * v + 1 ] = _verticesV[ v ];      
                arr[ 4 * v + 2 ] = _verticesU[s + 1];       // current curve
                arr[ 4 * v + 3 ] = _verticesV[ v ];
            }

            //
            _textureBuff[ s ] = gl.createBuffer( );
            gl.bindBuffer( gl.ARRAY_BUFFER, _textureBuff[ s ] );

            gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( arr ), gl.STATIC_DRAW );
            _textureBuff[ s ].itemSize = 2;
            _textureBuff[ s ].numItems = 2 * _vNum;
        }
    }
    
     /*****************************************
     *        drawNormal
     *  draw normal lines
     ****************************************/
    function drawNormal( )
    {
        createNormalLineBuff();
        var gl = Opengl.gl;
        gl.bindBuffer( gl.ARRAY_BUFFER, _normalLineBuff );
        gl.vertexAttribPointer( ShaderControl.shaderProgram.vertexPositionAttribute, _normalLineBuff.itemSize, gl.FLOAT, false, 0, 0 );
        
        gl.bindBuffer( gl.ARRAY_BUFFER, _normalLineBuff );
        gl.vertexAttribPointer( ShaderControl.shaderProgram.vertexNormalAttribute, _normalLineBuff.itemSize, gl.FLOAT, false, 0, 0 );
        
        gl.drawArrays( gl.LINES, 0, _normalLineBuff.numItems );
    };

    /*****************************************
     *        drawCurve
     *  draw curves
     ****************************************/
    function drawCurve( )
    {
        var gl = Opengl.gl;
        for ( var s = 0; s < _slices; s++ )
        {
            gl.bindBuffer( gl.ARRAY_BUFFER, _curveVertexBuff[ s ] );
            gl.vertexAttribPointer( ShaderControl.shaderProgram.vertexPositionAttribute, _curveVertexBuff[ s ].itemSize, gl.FLOAT, false, 0, 0 );
            
            gl.bindBuffer( gl.ARRAY_BUFFER, _curveVertexNormalBuff[ s ] );
            gl.vertexAttribPointer( ShaderControl.shaderProgram.vertexNormalAttribute, _curveVertexNormalBuff[ s ].itemSize, gl.FLOAT, false, 0, 0 );
            
            gl.drawArrays( gl.LINE_STRIP, 0, _curveVertexBuff[ s ].numItems );
        }
    }

    /*****************************************
     *        drawSurface
     *  draw surface
     ****************************************/
    function drawSurface( )
    {
        var gl = Opengl.gl;
        for ( var s = 0; s < _slices; s++ )
        {
            gl.bindBuffer( gl.ARRAY_BUFFER, _surfaceVertexBuff[ s ] );
            gl.vertexAttribPointer( ShaderControl.shaderProgram.vertexPositionAttribute, _surfaceVertexBuff[ s ].itemSize, gl.FLOAT, false, 0, 0 );

            gl.bindBuffer( gl.ARRAY_BUFFER, _textureBuff[ s ] );
            gl.vertexAttribPointer( ShaderControl.shaderProgram.textureCoordAttribute, _textureBuff[ s ].itemSize, gl.FLOAT, false, 0, 0 );
            
            gl.bindBuffer( gl.ARRAY_BUFFER, _surfaceVertexNormalBuff[ s ] );
            gl.vertexAttribPointer( ShaderControl.shaderProgram.vertexNormalAttribute, _surfaceVertexNormalBuff[ s ].itemSize, gl.FLOAT, false, 0, 0 );
            
            
            // decide draw wire or solid
            var mode = gl.LINE_STRIP;
            if(_isSolid)
            {
                mode = gl.TRIANGLE_STRIP;
            }
            
            gl.drawArrays( mode, 0, _surfaceVertexBuff[ s ].numItems );
        }
    }

     /*****************************************
     *          set texture
     ***************************************/
    this.setTextures = function(textures)
    {
        _textures = textures;
    };
    
    /*****************************************
     *          set texture
     ***************************************/
    this.getTextures = function()
    {
        return _textures;
    };
    /*****************************************
     *   initialize array buffer of vertices
     ***************************************/
    this.init = function()
    {
        createCurveVetexBuff();
        createSurfaceVetexBuff( );
        createTextureVetexBuff();
        createCurveVetexNormalBuff();
        createSurfaceVetexNormalBuff();
        
        
        if(_textures != null)
        {
            _textures.init();
        }
    };
    
    /*****************************************
     *          draw object
     ***************************************/
    this.draw = function( )
    {
        // draw curves themselves when in wire mode
        if(!_isSolid)
        {
            drawCurve();
            drawSurface();
        }
        // draw solid surface with texture
        else if(_textures != null)
        {
            _textures.enable();
            drawSurface( );
            _textures.disable();
        }
        // draw solid surface
        else
        {
            drawSurface( );
        }
        // draw normal lines
        if(_isDrawNormals)
        {
            drawNormal();
        }
    };
    
    /*****************************************
     *          dump (for debugging)
     ***************************************/
    function dump( )
    {
        Canvas.print( "Vertices" );
        for ( var s = 0; s < _slices + 1; s++ )
        {
            Canvas.print( "\n" + s + ": " );
            for ( var v = 0; v < _vNum; v++ )
            {
                Canvas.print( _vertices[s][ v ].toString( ) );
            }
        }
    };

};

