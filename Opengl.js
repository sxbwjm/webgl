 /**
  * Opengl class: Basic opengl operation
  * @author Xiaobo
  * Created: 11/18/2013
  */
var Opengl = new function()
{
	this.gl = null;
	
	this.mvMatrix = mat4.create();
    this.pMatrix = mat4.create();
    var mvMatrixStack = [];
    
    /*****************************************
	*   pass matrix uniforms to shader
	***************************************/
    this.setMatrixUniforms = function() 
    {
    	shaderProgram = ShaderControl.shaderProgram;
        this.gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, this.pMatrix);
        this.gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, this.mvMatrix);
        
        var pmvMatrix = mat4.create();
        mat4.multiply(this.pMatrix, this.mvMatrix, pmvMatrix);
        
        var normalMatrix = mat3.create();
        mat4.toInverseMat3(this.mvMatrix, normalMatrix);
        mat3.transpose(normalMatrix);
       
        this.gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, normalMatrix);
    };
    
    /*****************************************
	*   pass color uniform to shader
	***************************************/
    this.setColorUniforms = function(c) {
    	shaderProgram = ShaderControl.shaderProgram;
        this.gl.uniform4fv(shaderProgram.vColorUniform, c);
    };
    
    /*****************************************
	*   pass eye position uniform to shader
	***************************************/
    this.setEyePosUniforms = function(eyePos) {
    	shaderProgram = ShaderControl.shaderProgram;
        this.gl.uniform3fv(shaderProgram.eyePosUniform, eyePos);
    };
    
    /*****************************************
    *   create array buffer
    ***************************************/
    this.createArrayBuffer = function(vertices, itemSize, itemNum )
    {
         var buffer = this.gl.createBuffer( );
         this.gl.bindBuffer( this.gl.ARRAY_BUFFER, buffer );

         this.gl.bufferData( this.gl.ARRAY_BUFFER, new Float32Array( vertices ), this.gl.STATIC_DRAW );
         buffer.itemSize = itemSize;
         buffer.numItems = itemNum;
         
         return buffer;
    };
    /*****************************************
	*   push current matrix to stack
	***************************************/
    this.mvPushMatrix = function()
    {
    	var copy = mat4.create();
    	mat4.set(this.mvMatrix, copy);
    	mvMatrixStack.push(copy);
    };
    
    /*****************************************
	*   pop matrix from stack
	***************************************/
    this.mvPopMatrix = function()
    {
    	if(mvMatrixStack.length == 0)
    	{
    		throw "Invalid popMatrix!";
    	}
    	
    	this.mvMatrix = mvMatrixStack.pop();
    };
    
};
