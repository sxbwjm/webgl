/**
 * Fragment shader
 * @author Xiaobo Sun
 * Created: 11/18/2013
 */

precision mediump float;

/**********************************
*         uniform variables
**********************************/
uniform vec4 vColor;
//uniform sampler2D uTexture0;
//uniform sampler2D uTexture1;
uniform sampler2D uTextureIds[3];
uniform float uTextureAlphas[3];
uniform bool uTextureOnOffs[3];

uniform int uTextureNum;
uniform bool uUseTexture;

/**********************************
*         varying variables
**********************************/
varying vec2 vTextureCoord;
varying vec3 vLightWeight;

/**********************************
*               main
**********************************/
void main(void) 
{
    if(!uUseTexture)
    {
        gl_FragColor = vColor;
    }
    else
    {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
        
        float curAlpha = 0.0;
        float preAlpha = 0.0;
        // regular for loop using "i < uTextureNum" does not work
        for(int i=0; i<3; i++)
        {
            if(i < uTextureNum)
            {
                // test if the texuture is on
                if(uTextureOnOffs[i])
                {
                    curAlpha = uTextureAlphas[i];
                    gl_FragColor += texture2D(uTextureIds[i], vTextureCoord) * (1.0 - preAlpha) * curAlpha;
                    preAlpha = curAlpha;
                }
            }
        }
        
    }
    
   gl_FragColor = vec4(gl_FragColor.rgb * vLightWeight, gl_FragColor.a);
}