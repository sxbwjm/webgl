/**
 * Cube: basic class for all 3D shapes
 * 
 * @author Xiaobo Sun
 */
function Triangle(vertices)
{
	// inherit from Object3D class
    Object3D.call(this);

	var _vertices = vertices;
	
    var _triangleVertexPositionBuffer;
    
    /*****************************************
	*   initialize array buffer of vertices
	***************************************/
	this.init = function()
	{
    	var gl = Opengl.gl;
        _triangleVertexPositionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, _triangleVertexPositionBuffer);
      
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(_vertices), gl.STATIC_DRAW);
        _triangleVertexPositionBuffer.itemSize = 3;
        _triangleVertexPositionBuffer.numItems = 3;

   };
   
   /*****************************************
	*          draw triangle
	***************************************/
   this.draw = function()
   {
   		var gl = Opengl.gl;
   	   // initBuffers();
   	 	gl.bindBuffer(gl.ARRAY_BUFFER, _triangleVertexPositionBuffer);
        gl.vertexAttribPointer(ShaderControl.shaderProgram.vertexPositionAttribute, _triangleVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);
        gl.drawArrays(gl.TRIANGLES, 0, _triangleVertexPositionBuffer.numItems);
   };

}
