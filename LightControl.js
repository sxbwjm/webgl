/**
 * LightControl: manage multiple lights
 * 
 * @author Xiaobo Sun
 * @version 1.0 11/30/2013
 * 
 */

function LightControl()
{
    var _lights = [];

     createLights();
     resetLights();

    /**
     * create lights
     */
    function createLights()
    {
        // 1st light
        var light = new Light( 0 );
        _lights.push( light );

        // 2nd light
        light = new Light( 1 );
        _lights.push( light );
    }

    /**
     * get light by index
     * 
     * @param idx
     * @return
     */
    this.getLight = function( idx )
    {
        return _lights[idx];
    };

    /**
     * reset light to default values
     */
    function resetLights()
    {
        // 1st light (directional)
        var light = _lights[0];
        light.setAmbient( 0.2, 0.2, 0.2, 1.0 );
        light.setDiffuse( 0.8, 0.8, 0.8, 1.0 );
        light.setSpecular( 0.8, 0.8, 0.8, 1.0 );
        light.setPos( 0.0, 0.0, 1.0 );
        light.setDirectionLight();
        light.setEnable(true);

        // 2nd light (point)
        light = _lights[1];
        light.setAmbient( 0.4, 0.4, 0.4, 1.0 );
        light.setDiffuse( 0.8, 0.8, 0.8, 1.0 );
        light.setSpecular( 0.5, 0.5, 0.5, 1.0 );
        light.setPointLight();
        light.setPos( 1.0, 0.0, 0.0 );
        light.setEnable(true);
        
    };

    /**
     * apply all lights effect
     */
    this.update = function()
    {
        for (var i=0; i<_lights.length; i++ )
        {
            _lights[i].update();
        }
    };
}

 